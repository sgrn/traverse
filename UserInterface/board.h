/**
 * \file board.h
 * \brief This file contains headers of objects that perform graphical text operation on the game board.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-04-24 Fri 03:41 PM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __BOARD_H__
	#define __BOARD_H__

	#include <ncurses.h>
	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include <errno.h>
	#include <stdbool.h>

	#include "start_menu.h"

	typedef struct {
		WINDOW * squares[BOARD_SQUARE_HEIGHT][BOARD_SQUARE_WIDTH];
		WINDOW * whole;
	} Board;

	typedef enum {
		PLAYER_0 = '#',
		PLAYER_1,
		PLAYER_2,
		PLAYER_3,
		NOBODY
	} Players;

	typedef struct {
		int currentY;
		int currentX;
		int savedY;
		int savedX;
	} BoardCursor;

	/**
	 * \fn Board * Board_allocate(const int atY, const int atX)
	 * \brief Allocates a \a Board object.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:03 AM
	 * \param atY y Coordinate at of the window at which the board starts.
	 * \param atX x Coordinate at of the window at which the board starts.
	 * \return Newly allocates \a Board object.
	 */
	Board * Board_allocate(const int atY, const int atX);

	/**
	 * \fn void Board_check_screensize(void)
	 * \brief Checks screen size and ask to resize the terminal window if it is too small for the board.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:05 AM
	 */
	void Board_check_screensize(void);

	/**
	 * \fn static short Board_color(int y, int x)
	 * \brief Returns color value for a given position within the board.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:07 AM
	 * \param y Y height.
	 * \param x X width.
	 * \return color pair value.
	 */
	static short Board_color(int y, int x);

	/**
	 * \fn Players Players_symbol_for(const size_t index)
	 * \brief Returns player symbol for a given player ID.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:10 AM
	 * \param index Player ID.
	 * \return Player symbol.
	 */
	Players Players_symbol_for(const size_t index);

	/**
	 * \fn size_t Players_index_for(const Players symbol)
	 * \brief Returns player index for a given player symbol.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-24 Sun 10:26 PM
	 * \param symbol Given Player symbol.
	 * \return Player index.
	 */
	size_t Players_index_for(const Players symbol);

	/**
	 * \fn static void Square_draw_empty(Board * const board, const int i, const int j)
	 * \brief Draws an empty square at a given position within the board.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:13 AM
	 * \param board A given \a Board object.
	 * \param i A given height.
	 * \param j A given width.
	 */
	static void Square_draw_empty(Board * const board, const int i, const int j);

	/**
	 * \fn void Square_draw_square(Board * const board, const int y, const int x, const Players symbol)
	 * \brief Draws a square at a given position within the board.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:20 AM
	 * \param board A given \a Board object.
	 * \param y A given height.
	 * \param x A given width.
	 * \param symbol Given player symbol.
	 */
	void Square_draw_square(Board * const board, const int y, const int x, const Players symbol);
	
	/**
	 * \fn void Square_draw_circle(Board * const board, const int y, const int x, const Players symbol)
	 * \brief Draws a circle at a given position within the board.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:20 AM
	 * \param board A given \a Board object.
	 * \param y A given height.
	 * \param x A given width.
	 * \param symbol Given player symbol.
	 */
	void Square_draw_circle(Board * const board, const int y, const int x, const Players symbol);

	/**
	 * \fn void Square_draw_triangle(Board * const board, const int y, const int x, const Players symbol)
	 * \brief Draws a triangle at a given position within the board.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:20 AM
	 * \param board A given \a Board object.
	 * \param y A given height.
	 * \param x A given width.
	 * \param symbol Given player symbol.
	 */
	void Square_draw_triangle(Board * const board, const int y, const int x, const Players symbol);

	/**
	 * \fn void Square_draw_diamond(Board * const board, const int y, const int x, const Players symbol)
	 * \brief Draws a diamond at a given position within the board.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:20 AM
	 * \param board A given \a Board object.
	 * \param y A given height.
	 * \param x A given width.
	 * \param symbol Given player symbol.
	 */
	void Square_draw_diamond(Board * const board, const int y, const int x, const Players symbol);

	/**
	 * \fn void Board_init(Board * const board)
	 * \brief Initializes a \a Board object.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:24 AM
	 * \param board A given \a Board object.
	 */
	void Board_init(Board * const board);

	/**
	 * \fn Board * Board_generate(const StartupSettings settings)
	 * \brief Generates a \a Board object.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:36 AM
	 * \param settings Startup settings.
	 * \return Newly allocates \a Board object.
	 * Generates means that the object is prepared to be used.
	 */
	Board * Board_generate(const StartupSettings settings);


	/**
	 * \fn static void Board_init_sides(Board * const board, const NumberOfPlayer n)
	 * \brief Initialized all player pieces on the sides of the board.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:38 AM
	 * \param board A given \a Board object.
	 * \param n Numbers of players.
	 */
	static void Board_init_sides(Board * const board, const NumberOfPlayer n);

	/**
	 * \fn void BoardCursor_unselect(BoardCursor * const cursor)
	 * \brief Sets a given \a BoardCursor in an unselect state.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:41 AM
	 * \param cursor A given \a BoardCursor object.
	 */
	void BoardCursor_unselect(BoardCursor * const cursor);

	/**
	 * \fn void BoardCursor_unselect(BoardCursor * const cursor)
	 * \brief Initializes a \a BoardCursor object.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:41 AM
	 * \param cursor A given \a BoardCursor object.
	 */
	void BoardCursor_init(BoardCursor * const cursor);

	/**
	 * \fn void BoardCursor_unselect(BoardCursor * const cursor)
	 * \brief Sets a given \a BoardCursor in a select state.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:41 AM
	 * \param cursor A given \a BoardCursor object.
	 */
	void BoardCursor_select(BoardCursor * const cursor);

	/**
	 * \fn bool BoardCursor_isselected(BoardCursor * const cursor)
	 * \brief Tests if a given \a BoardCursor is in a select state.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:48 AM
	 * \param cursor A given \a BoardCursor object.
	 * \return true if cursor in select else false.
	 */
	bool BoardCursor_isselected(BoardCursor * const cursor);

	/**
	 * \fn void BoardCursor_move(BoardCursor * const cursor, const int y, const int x)
	 * \brief Sets cursor position.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:48 AM
	 * \param cursor A given \a BoardCursor object.
	 * \param y A given height.
	 * \param x A given width.
	 */
	void BoardCursor_move(BoardCursor * const cursor, const int y, const int x);

	/**
	 * \fn void BoardCursor_move_up(BoardCursor * const cursor)
	 * \brief Moves up the cursor position.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:48 AM
	 * \param cursor A given \a BoardCursor object.
	 */
	void BoardCursor_move_up(BoardCursor * const cursor);
	
	/**
	 * \fn void BoardCursor_move_right(BoardCursor * const cursor)
	 * \brief Moves to the right the cursor position.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:48 AM
	 * \param cursor A given \a BoardCursor object.
	 */
	void BoardCursor_move_right(BoardCursor * const cursor);

	/**
	 * \fn void BoardCursor_move_down(BoardCursor * const cursor)
	 * \brief Moves down the cursor position.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:48 AM
	 * \param cursor A given \a BoardCursor object.
	 */
	void BoardCursor_move_down(BoardCursor * const cursor);

	/**
	 * \fn void BoardCursor_move_left(BoardCursor * const cursor)
	 * \brief Moves to the left the cursor position.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:48 AM
	 * \param cursor A given \a BoardCursor object.
	 */
	void BoardCursor_move_left(BoardCursor * const cursor);

	/**
	 * \fn void Board_free(Board * * const board)
	 * \brief Frees a given board object.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:49 AM
	 * \param board Address of a pointer of a \Board object.
	 */
	void Board_free(Board * * const board);
	
	/**
	 * \fn void Board_move_piece(Board * const board, const int y0, const int x0, const int y1, const int x1)
	 * \brief Moves graphically a piece from a given position to another given position.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:49 AM
	 * \param board A given \a Board object.
	 * \param y0 A given height (current position).
	 * \param x0 A given width (current position).
	 * \param y1 A given height (new position).
	 * \param x0 A given width (new position).
	 */
	void Board_move_piece(Board * const board, const int y0, const int x0, const int y1, const int x1);

	/**
	 * \fn void Board_move(Board * const board, const int y, const int x)
	 * \brief Moves cursor graphically to a given position.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:49 AM
	 * \param board A given \a Board object.
	 * \param y A given height.
	 * \param x A given width.
	 */
	void Board_move(Board * const board, const int y, const int x);

	/**
	 * \fn void Board_restore(Board * const board, const int y, const int x)
	 * \brief Removes cursor persistent frame.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:49 AM
	 * \param board A given \a Board object.
	 * \param y A given height.
	 * \param x A given width.
	 */
	void Board_restore(Board * const board, const int y, const int x);

	/**
	 * \fn void Board_select(Board * const board, const int y, const int x)
	 * \brief Selects piece graphically at a given position.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:50 AM
	 * \param board A given \a Board object.
	 * \param y A given height.
	 * \param x A given width.
	 */
	void Board_select(Board * const board, const int y, const int x);

	/**
	 * \fn void Board_unselect(Board * const board, const int y, const int x)
	 * \brief Unselects piece graphically at a given position.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 08:50 AM
	 * \param board A given \a Board object.
	 * \param y A given height.
	 * \param x A given width.
	 */
	void Board_unselect(Board * const board, const int y, const int x);

#endif

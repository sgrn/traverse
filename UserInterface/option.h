/**
 * \file option.h
 * \brief This files contains headers of routines related to status bar options.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-05-03 Mon 10:09 AM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __OPTION_H__
	#define __OPTION_H__

	#include <ncurses.h>
	#include <stdio.h>
	#include <stdlib.h>

	#include "../Core/terminal_io.h"
	#include "../Core/save.h"

	/**
	 * \fn void Option_help(WINDOW * const bar, WINDOW * const board)
	 * \brief Displays help during the game.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 10:14 AM
	 * \param bar A status bar \a WINDOW.
	 * \param board A status board \a WINDOW.
	 */
	void Option_help(WINDOW * const bar, WINDOW * const board);

	/**
	 * \fn void Option_save(WINDOW * const bar, WINDOW * const board, const CoreBoard * const coreBoard, const size_t currentPlayerIndex, const size_t roundCounter, const NumberOfPlayer numberOfPlayer, const short botNumber, const short difficulty)
	 * \brief Displays save menu.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-28 Thu 02:20 AM
	 * \param bar A status bar \a WINDOW.
	 * \param board A status board \a WINDOW.
	 * \param coreBoard A \s CoreBoard object.
	 * \param currentPlayerIndex Current player index.
	 * \param roundCounter Round counter.
	 * \param numberOfPlayer Number of player.
	 * \param botNumber Bot number.
	 * \param difficulty Bot difficulty.
	 */
	void Option_save(WINDOW * const bar, WINDOW * const board, const CoreBoard * const coreBoard, const size_t currentPlayerIndex, const size_t roundCounter, const NumberOfPlayer numberOfPlayer, const short botNumber, const short difficulty);

#endif

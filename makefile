compiler = gcc
objects = main.o list.o vector.o coreboard.o minimax.o save.o terminal_io.o \
board.o event.o interface.o option.o start_menu.o status_bar.o

traverse : $(objects)
	$(compiler) -o traverse $(objects) -lncurses

main.o : main.c UserInterface/interface.h
	$(compiler) -c main.c

coreboard.o : Core/coreboard.c Core/coreboard.h UserInterface/board.h UserInterface/start_menu.h \
Lib/List/list.h Lib/Vector/vector.h
	$(compiler) -c Core/coreboard.c

minimax.o : Core/minimax.c Core/minimax.h Core/coreboard.h \
Lib/List/list.h Lib/Vector/vector.h
	$(compiler) -c Core/minimax.c

save.o : Core/save.c Core/save.h Core/coreboard.h UserInterface/board.h \
Core/terminal_io.h
	$(compiler) -c Core/save.c

terminal_io.o : Core/terminal_io.c Core/terminal_io.h
	$(compiler) -c Core/terminal_io.c

board.o : UserInterface/board.c UserInterface/board.h  UserInterface/start_menu.h
	$(compiler) -c UserInterface/board.c

event.o : UserInterface/event.c UserInterface/event.h  UserInterface/board.h UserInterface/status_bar.h
	$(compiler) -c UserInterface/event.c

interface.o : UserInterface/interface.c UserInterface/interface.h  UserInterface/board.h UserInterface/status_bar.h \
Lib/List/list.h UserInterface/event.h UserInterface/start_menu.h UserInterface/option.h Core/coreboard.h Core/minimax.h
	$(compiler) -c UserInterface/interface.c

option.o : UserInterface/option.c UserInterface/option.h Core/terminal_io.h
	$(compiler) -c UserInterface/option.c

start_menu.o : UserInterface/start_menu.c UserInterface/start_menu.h Core/terminal_io.h
	$(compiler) -c UserInterface/start_menu.c

status_bar.o : UserInterface/status_bar.c UserInterface/status_bar.h UserInterface/board.h
	$(compiler) -c UserInterface/status_bar.c

vector.o : Lib/Vector/vector.h Lib/Vector/vector.c
	$(compiler) -c Lib/Vector/vector.c

list.o : Lib/List/list.h Lib/List/list.c
	$(compiler) -c Lib/List/list.c

.PHONY : clean
clean :
	rm traverse $(objects)
